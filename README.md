# README #

This repository contains some functionalities to correct, update and manipulate 3D arrays of numerical data representing a "hypercube", i.e. a hyperspectral picture of a geological scene or object.

[Eric Parent](mailto:eric@eparent.info)
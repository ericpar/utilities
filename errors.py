#!/usr/bin/env python
#

'''Handles the error codes and tracing during execution.


Dependencies
------------

This package is parsing error codes from 'errors.csv' and makes them
available for errors handling.


Author: Eric Parent <eric@eparent.info>

Copyright (c) 2013 Eric Parent

License: The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

'''

# ------------------------------
# Standard libraries import
# ------------------------------
import os
import sys

# ------------------------------
# Custom libraries
# ------------------------------
from utils.Log import Logger

ErrCodesFile = 'errors.csv'

global __error_messages
global __error_codes
global __loaded

__loaded = False
__error_codes = None
__error_messages = None


def _capitalize(txt):
    '''
    If text given is "a white fox is not brown", then the output is
    "AWhiteFoxIsNotBrown".
    '''
    parts = txt.strip().split()
    return ''.join(map(lambda x: x.capitalize(), parts))


def __parse():
    '''
    Parsing of the ErrCodesFile file which holds the error codes and
    messages.
    '''
    global __loaded
    global __error_messages
    global __error_codes
    if not __loaded:
        __error_messages = {}
        __error_codes = {}
        error_file = None
        try:
            pwd = os.path.dirname(os.path.realpath(__file__))
            filename = os.path.join(pwd, ErrCodesFile)
            error_file = open(filename, 'r')
            for line in error_file:
                _code, _msg = line.split(',')
                code = int(_code)
                msg = _msg.strip()
                if msg:
                    __error_messages[code] = msg
                    __error_codes[_capitalize(msg)] = code
        finally:
            if error_file is not None:
                error_file.close()
        __loaded = True
    return __loaded, __error_messages, __error_codes


def parse_codes():
    '''
    Parsing of the ErrCodesFile file which holds the error codes and
    messages.

    Returns a dictionnary that gives the error message based on a Key
    (error code, integer).
    '''
    _parsed, _err_msg, _err_codes = __parse()
    return _err_codes


def ErrorCodes(desc, msg='', log=None, fatal=False):
    '''Function that takes care of the logging and printing of an error message
    given an error description.

    Arguments:
    ----------

    - desc: a textual description of the error (has to be already defined
    in the 'ErrCodesFile' file).

    - msg: an extra message to be shown in the error message (printed
    and logged)

    - log: logger to be used if not None

    - fatal: boolean indicating if it's a fatal error or not

    Returns the error code associated with the error description.

    Post-conditions:
    ----------------

    - logging of the error is done

    - Error message is printed to the stdout

    '''
    _parsed, _err_msg, _err_codes = __parse()

    if isinstance(desc, str):
        # Given the error description
        _errCode = _err_codes[desc]
        _errMsg = _err_msg[_errCode]
    elif isinstance(desc, int):
        # Given the error code
        _errCode = desc
        _errMsg = _err_msg[_errCode]

    if _errMsg != 'Success':
        # It is actually an error, not a Success
        if msg:
            _msg = "%s : %s" % (_errMsg, str(msg))
        else:
            _msg = _errMsg
        print "ERROR -- " + _msg
        if log is None:
            log = Logger()
        if fatal:
            log.fatal(_msg, tab=0)
            sys.exit(_errCode)
        else:
            log.error(_msg, tab=0)
    return _errCode


Success = 0   # default value
# overwrite if any is defined
try:
    Success = parse_codes()['Success']
except KeyError:
    pass


#
# --- Unit tests for parser ---
#
if __name__ == "__main__":
    import unittest
    class Test(unittest.TestCase):
        def test_basic(self):
            print
            try:
                err_codes = parse_codes()
                print "Success value: %d" % Success
                print "ErrorCodes:"
                for key in err_codes.keys():
                    code = err_codes[key]
                    print "key : %s" % str(key)
                    ErrorCodes(key)
                    ErrorCodes(code)
                    ErrorCodes(code, msg='unit testing')

            except Exception, exc:
                self.fail("Could not parse the file: %s" % exc)

    unittest.main()

#!/usr/bin/env python
# -*- coding: latin-1 -*-
'''Provides a basic yet functional implementation of an "enum" class
that mimics the behaviour of a C/C++ enum construction.

Author: Eric Parent
Email: eparent@photonicknowledge.com

This code is a modified version of what's available here:
http://stackoverflow.com/questions/702834/whats-the-common-practice-for-enums-in-python


Copyright (c) 2013 Eric Parent

License: The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

'''


class Enumeration(object):
    '''
    Implements an Enumeration class which is a class that has
    attributes associated with an integer value.

    Example:

    >>> from enumeration import Enumeration
    >>> Foo = Enumeration('toto', 'patate', 'weird')
    >>> Foo.toto
    0
    >>> Foo.weird
    2
    >>> Fruits = Enumeration('banana', 'peaches', 'apple', start = 2 )
    >>> Fruits.apple
    4
    >>> Fruits.banana
    2
    '''
    def __init__(self, *names, **kwargs):
        '''
        Optional named parameter:
        start (default to 1)
        '''
        kwds = {'start': 0}
        kwds.update(kwargs)
        start = kwds['start']
        for number, name in enumerate(names):
            setattr(self, name, number + start)


#
# --- Basic unit testing ---
#
if __name__ == '__main__':
    import doctest
    doctest.testmod()

#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""This module holds a few functions to enable with the affine
transformations of 2D images.

Dependencies
------------

- Numpy

- SciPy


Author: Eric Parent <eric@eparent.info>

Copyright (c) 2012 Eric Parent

License: The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
# --------------------
# Standard libraries
#--------------------
import numpy as np
import scipy.ndimage as nd

# ------------------------------
# Third party library imports
# ------------------------------
import numpy.linalg as lin
import numpy as np


def invert_coords(points):
    """Inverts the coordinates of the points so the X and Y axis are
    inverted.

    Arguments
    ---------

    - points: a list of points to be inverted

    """
    inverted = np.array([(p[-1], p[0]) for p in points])
    return inverted


def affine_components(matrix):
    """Returns the parts of the affine transformation matrix that are
    to be used in the transformation.

    Arguments
    ---------

    - matrix: the matrix that holds all the affine transformation
      information

    Return (A, b), the multiplicative and additive portions of the
    affine transformation.

    """
    # Extracting the geometric transformation part from the matrix
    A = matrix[:2, :2]

    # Extracting constant offset from the matrix
    b = np.array((matrix[0, 2], matrix[1, 2]))
    return (A, b)


def apply_transformation(img, matrix):
    """Applying the affine transformation described by M to the image
    'img' and returning a new image

    Arguments
    ---------

    - img: the image on which to apply the transformation

    - matrix: the matrix of the affine transformation to be applied

    NOTE:
    -----

    The affine transformation matrix M is of the following form:

    M = [[A, B, C],
         [D, E, F],
         [0, 0, 1]]

    """
    A, b = affine_components(matrix)

    # Output image of same shape as the input
    shape = img.shape
    out = np.zeros(img.shape)

    # Looping for each layer of the image, for the case of images with
    # composite pixel values, like A, R, G, B
    if len(shape) == 3:
        for k in range(out.shape[2]):
            out[:, :, k] = nd.interpolation.affine_transform(img[:, :, k], A, b)
    else:
        out = nd.interpolation.affine_transform(img, A, b)
    return out


def transform_single_point(matrix, point):
    """Applies an affine transformation to a single point.

    Arguments
    ---------

    - matrix: the affine transformation matrix

    - point: a pair of coordinates (list)

    Returns the modified point.

    """
    A, b = affine_components(matrix)
    _point = np.array(np.dot(A, np.array(point)) + b, dtype='int32')
    return _point


def transform_points(matrix, points):
    """Computes the new mask points based on the given affine
    transformation matrix.

    Arguments:
    ----------

    - matrix: the affine transformation matrix to be used

    - points: a list of triplets being the mask_pts in the source
      image

    NOTE:
    -----

    The 'matrix' transformation matrix is expected to be of the
    following shape:

    M = [[A, B, C],
         [D, E, F],
         [0, 0, 1]]

    where [[A, B], [D, E]] and [C, F] are corresponding to the
    geometrical and constant (offset) parts of the affine
    transformation respectively.

    Returns a list of triplets being the mask_pts to be used in the
    target image.

    """
    targets = []
    for box in points:
        _box = [transform_single_point(matrix, coord) for coord in box]
        targets.append(_box)
    return np.array(targets)


def affine_matrix_from_points(ptsSource, ptsTarget):
    """Given a set of points present in a source image and their
    corresponding points in a target image, compute the affine
    transformation matrix M.

    References
    ----------

    (1) Web page "Les transformations affines (avec NumPy)" available
    at this URL:
    http://www.portailsig.org/content/les-transformations-affines-avec-numpy-ou-la-signification-geometrique-d-un-fichier-worldfil

    NOTES
    -----

    Let (x1, y1), (x2, y2), ..., (xn, yn) be coordinates in the source
    image.

    Let (x1', y1'), (x2', y2'), ..., (xn', yn') be the corresponding
    coordinates in the target image.

    Then we're interested in getting the affine transformation matrix
    that satisfies the following equation:

    x1', x2', ..., xn'   = M * x1, x2, ..., xn    (1)
    y1', y2', ..., yn'         y1, y2, ..., yn

    For brievity, let

    x1', x2', ..., xn'   = tp                     (2)
    y1', y2', ..., yn'

    and let

    x1, x2, ..., xn      = fp                     (3)
    y1, y2, ..., yn
    0,  0,  ..., 1

    And the equation (1) can be re-written like this, using (2) and
    (3):

    tp = M * fp                                   (4)

    Than we can compute M in the following manner:

    M = tp * fp.I                                 (5)

    Arguments:
    ----------
    - ptsSource: a list of coordinates of points in the source image,
      i.e. the image before any affine transformation.
    - ptsTarget: a list of coordinates of points in the target image
      that are matching the points in ptsSource after the affine
      transformation is applied.

    EXAMPLE:
    -------

    >>> q = [(66, 217), (74, 953), (1288, 962)]
    >>> w = [(1137, 1095), (1164, 2061), (2784, 2061)]
    >>> W = affine_matrix_from_points(q, w)
    >>> Y = W[:2, :2]       # extracting the geometric transformation
    >>> r = np.array((W[0, 2], W[1, 2]))  # extracting the offset
    >>> for i, u in enumerate(q):
    ...   np.array(np.dot(Y, np.array(u)) + r, dtype='int32')
    ... 
    array([1137, 1095])
    array([1164, 2061])
    array([2784, 2061])

    """
    target = [np.array(p, dtype='float32').transpose() for p in ptsTarget]
    source = [np.array(p, dtype='float32').transpose() for p in ptsSource]

    # Add this neutral line for having homogenous coordinates
    neutral = np.array([1] * len(source), dtype='float32')
    upper = np.matrix(np.vstack(source).transpose())
    fp = np.matrix(np.vstack((upper, neutral)))
    tp = np.matrix(np.vstack(target).transpose())
    # Compute the actual transformation matrix
    M = tp * fp.I
    return np.asarray(M)


def compute_affine_transformation(points, matrix):
    """Returns a list of computed points based on the M affine
    transformation matrix. This function takes care of returning a
    list of coordinates that have the same shape as it was given.

    Arguments:
    - points: the points to be transformed
    - matrix: the affine transformation matrix

    Example
    -------

    >>> pointsRGB = [(1137, 1095), (1164, 2061), (2784, 2061)]
    >>> pointsHI = [(66, 217), (74, 953), (1288, 962)]
    >>> M = affine_matrix_from_points(pointsHI, pointsRGB)
    >>> compute_affine_transformation(pointsHI, M)
    [(1137, 1095), (1164, 2061), (2784, 2061)]

    """
    computed = []
    if len(points) > 0:
        pts_shape = (len(points[0]), 1)
        for p in points:
            p = np.matrix(list(p) + [1]).transpose()
            v = np.asarray(matrix * p)
            w = tuple(np.array(v, dtype='int32').reshape(-1,))
            computed.append(w)
    return computed


def rotation_angle(pointsA, pointsB, degres=False):
    """Computes the rotation angle from pointsA and representation of
    pointsB after being linealy transformed.

    Arguments:
    - pointsA: a list of three points in fixed image
    - pointsB: a list of three points in the target image
    - degres: optional argument for determining if angle needs to be
      in degrees. Default is radians.

    Returns an angle between the axis depicted in pointsB with respect
    to axis depicted in pointsA.

    NOTES
    -----

    To improve the accuracy of the rotation angle from pointsA to
    pointsB, it is a good idea to pre-scale the pointsB so they match
    the order of magnitude of pointsA.

    The angle computation uses the vector produc, also called "dot
    product" due to its notation '.'.

    The law of vectorial products is the following:

    U . V = |U| x |V| x cos(t)

    where t is the angle between U and V.

    Therefore, t = arccos((U . V) / (|U| x |V|))

    Example:
    --------

    >>> pointsA = ((1, 4), (1, 1), (7, 1))
    >>> pointsB = ((1, 4), (0, 0), (7, 0))
    >>> pointsC = ((0, 7), (0, 0), (4, 4))
    >>> rotation_angle(pointsA, pointsB, degres=False)
    3.1415926535897931
    >>> rotation_angle(pointsA, pointsC, degres=True)
    45.0

    """
    dtype = 'int32'
    # A is upper-left corner,
    # B is lower-left corner and
    # C is lower-right corner
    A, B, C = pointsA
    U = np.array(C, dtype=dtype) - np.array(B, dtype=dtype)

    # P is upper-left corner,
    # Q is lower-left corner and
    # R is lower-right corner
    P, Q, R = pointsB
    V = np.array(Q, dtype=dtype) - np.array(R, dtype=dtype)

    # Compute the angle based on the vectorial product definition
    angle = np.arccos(np.dot(U, V) / (lin.norm(U) * lin.norm(V)))

    # Returning the angle in degrees
    if degres:
        angle = 180.0 - (angle * 180.0 / np.pi)
    return angle


#
# Standalone version of this script
#
if __name__ == '__main__':
    import doctest
    doctest.testmod()

#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""
INSTRUCTIONS:

 - Have all the Excel files to be merged in a single directory.

 - All files to be merged need to follow this naming convention:
     #<filename>.xlsx

 - Open a Windows command prompt:
        Start Menu -> All programs -> Command Prompt

 - Go into the directory where the stitcher.py script lives
        cd /path/to/my/script

 - launch the scipt with the full path to where the Excel files
   are located:
        python stitcher.py -D /path/to/excel/files

 - As a result, you'll have a file "merged.xlsx" created with the
   content of all the scanned Excel files merged, tab by tab.


Dependencies
------------

- openpyxl


Author: Eric Parent <eric@eparent.info>

Copyright (c) 2013 Eric Parent

License: The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

# -----------------------------
# Standard library imports
# -----------------------------
import os
import sys
import re
import openpyxl as xl
import argparse

# -----------------------------
# Arguments Parser
# -----------------------------
parser = argparse.ArgumentParser(description="Stitches various Excel of the same format")

parser.add_argument('-D', '--directory',
                    action="store",
                    type=str,
                    default="",
                    dest='directory',
                    help='directory where Excel files are')

parser.add_argument('--horizontal',
                    action="store_true",
                    dest='horizontal',
                    help="Stitching to be done in the horizontal axes, i.e. along each row")

parser.add_argument('--usage',
                    action="store_true",
                    dest='usage',
                    help="Shows some instructions")


def help():
    print __doc__


def usage():
    """Describes the use of this script."""
    print     """Stiching help

    Performs the stitching of various Excel files that are of the same
    format, i.e. same tabs structure (name and content)

    Options:

    -D, --directory    directory having the Excel files to stitch

    -Y, --horizontal   if defined, the stitching will be along the lines instead
                       of along the columns

    --usave            shows an example of use
    """


def list_files(work_dir, regex=r'#.*\.xlsx'):
    pattern = re.compile(regex)
    return [os.path.join(work_dir, f) \
            for f in os.listdir(work_dir) if pattern.match(f)]


def _merge_horizontal(source_sheet, merged_sheet):
    """Merges the content of the source_sheet in the 'merged_sheet' by
    adding columns after columns.

    Arguments:
    ----------

    - source_sheet: source worksheet

    - merged_sheet: destination worksheet

    Post-condition:
    ---------------

    The 'merged_sheet' is updated with the content of
    'source_sheet'.

    """
    current_row = merged_sheet.get_highest_row()
    current_col = merged_sheet.get_highest_column()
    for j in xrange(source_sheet.get_highest_row()):
        for k in xrange(source_sheet.get_highest_column()):
            x = j
            y = current_col + k
            target_cell = merged_sheet.cell(row=x, column=y)
            source_cell = source_sheet.cell(row=j, column=k)
            val = source_cell.value
            target_cell.bind_value(val)


def _merge_vertical(source_sheet, merged_sheet):
    """Merges the content of the source_sheet in the 'merged_sheet' by
    adding lines after lines.

    Arguments:
    ----------

    - source_sheet: source worksheet

    - merged_sheet: destination worksheet

    Post-condition:
    ---------------

    The 'merged_sheet' is updated with the content of
    'source_sheet'.

    """
    for row in source_sheet.rows:
        merged_sheet.append([r.value for r in row])


def merge(**kwargs):
    """Merges some Excel files based on the given arguments."""
    working_dir = kwargs['directory']
    horizontal = 'horizontal' in kwargs and kwargs['horizontal']
    if horizontal:
        merge_fn = _merge_horizontal
    else:
        merge_fn = _merge_vertical
    filenames = list_files(working_dir)
    print "files to merge:"
    for f in filenames:
        print f
    print
    output_file = os.path.join(working_dir, "merged.xlsx")
    merged = xl.workbook.Workbook()
    created_sheets = []
    for filename in filenames:
        print "-- Handling file '%s'" % filename
        origin = xl.load_workbook(filename=filename)
        for sheet_name in origin.get_sheet_names():
            source_sheet = origin.get_sheet_by_name(sheet_name)
            if sheet_name not in created_sheets:
                worksheet = merged.create_sheet()
                worksheet.title = sheet_name
                created_sheets.append(sheet_name)
            else:
                worksheet = merged.get_sheet_by_name(name=sheet_name)
            merge_fn(source_sheet, worksheet)
    merged.save(output_file)
    return 0


#
# Standalone version of this script
#
if __name__ == "__main__" :
    try:
        args = vars(parser.parse_args())
    except argparse.ArgumentError:
        usage()
        sys.exit(-1)

    if 'usage' in args and args['usage']:
        help()
        sys.exit(0)
    elif 'directory' in args and not args['directory']:
        usage()
    else:
        work_dir = args['directory']
        print "work_dir: ", work_dir
        sys.exit(merge(**args))

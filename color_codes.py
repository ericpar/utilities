#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""This script saves color squares of 100x100 pixels for the number of
items per range of possible pixel values or each (R, G, B) channel.


Dependencies
------------

- NumPy

- SciPy


Copyright (C) 2012 Eric Parent

License: The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

import os.path

from scipy.misc import imsave
import numpy as np


limit = 256

def save_squares(nb_squares=1):
    a = np.zeros((100, 100, 3))
    for r in np.linspace(0, limit, nb_squares):
        r = int(r)
        for g in np.linspace(0, limit, nb_squares):
	          g = int(g)
            for b in np.linspace(0, limit, nb_squares):
	              b = int(b)
                a[:, :, 0] = r
                a[:, :, 1] = g
                a[:, :, 2] = b
                filename = "%03d%03d%03d.png" % (r, g, b)
                print "saving %s" % os.path.abspath(filename)
                imsave(filename, a)
    print
    print "done!"
    print

if __name__ == "__main__":
    save_squares(step=10)

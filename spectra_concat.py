#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""Performs the stitching of columns of information from various CSV
(possibly named *.spectrum) files into a single one.


INSTRUCTIONS
------------

 - Have all the Excel files to be merged in a single directory.

 - All files to be merged need to follow this naming convention:
     #<filename>.xlsx

 - Open a Windows command prompt:
        Start Menu -> All programs -> Command Prompt

 - Go into the directory where the stitcher.py script lives
        cd /path/to/my/script

 - launch the scipt with the full path to where the Excel files
   are located:
        c:\Python27\python.exe spectra_concat.py -D /path/to/spectrum/files

 - As a result, you'll have a file "merged.xlsx" created with the
   content of all the scanned Excel files merged, tab by tab.


Author: Eric Parent <eric@eparent.info>

Copyright (c) 2013 Eric Parent

License: The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

# -----------------------------
# Standard library imports
# -----------------------------
import os
import sys
import re
import csv
import argparse

# -----------------------------
# Arguments Parser
# -----------------------------
parser = argparse.ArgumentParser(description="Stitches various Excel of the same format")

parser.add_argument('-D', '--directory',
                    action="store",
                    type=str,
                    default="",
                    dest='directory',
                    help='directory where Excel files are')

parser.add_argument('--horizontal',
                    action="store_true",
                    dest='horizontal',
                    help="Stitching to be done in the horizontal axes, i.e. along each row")

parser.add_argument('--usage',
                    action="store_true",
                    dest='usage',
                    help="Shows some instructions")


def help():
    print __doc__


def usage():
    """Describes the use of this script."""
    print     """Stiching help

    Performs the stitching of various Excel files that are of the same
    format, i.e. same tabs structure (name and content)

    Options:

    -D, --directory    directory having the Excel files to stitch

    -Y, --horizontal   if defined, the stitching will be along the lines instead
                       of along the columns

    --usave            shows an example of use
    """


def list_files(work_dir, regex=r'.*\.spectrum$'):
    pattern = re.compile(regex)
    return [os.path.join(work_dir, f) \
            for f in os.listdir(work_dir) if pattern.match(f)]


def _read_spectrum(filename, values):
    # filename = r"T:\eparent\Test Au\Au_5_3.spectrum"
    data = []
    wavelengths = []
    with open(filename, 'rb') as csvfile:
        # default value for the tag
        tag = os.path.basename(filename).split('.')[0]
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            if row[0].startswith(';'):
                if 'TAG' in row:
                    tag = row.split("=")[-1].strip()
                    print 'tag:', tag
            else:
                wavelengths.append(float(row[0]))
                data.append(float(row[-1]))
    values[tag] = data
    if 'wavelengths' not in values:
        values['wavelengths'] = wavelengths
    return values


def write_csv(filename, entries):
    """
    """
    headers = ['wavelengths']
    data = [entries.pop('wavelengths')]
    headers.extend(entries.keys())
    data.extend(entries.values())
    # write lines of data
    with open(filename, 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=",")
        # Write headers
        writer.writerow(headers)
        # Write numerical data
        for line in zip(*data):
            writer.writerow(line)
    return filename


def merge(**kwargs):
    """Merges some Excel files based on the given arguments."""
    working_dir = kwargs['directory']
    filenames = list_files(working_dir, regex=r'.*\.spectrum$')
    output_file = os.path.join(working_dir, "merged.csv")
    info = {}
    for filename in filenames:
        print "-- reading file '%s'" % filename
        info = _read_spectrum(filename, info)
    write_csv(output_file, info)
    return 0


#
# Standalone version of this script
#
if __name__ == "__main__" :
    try:
        args = vars(parser.parse_args())
    except argparse.ArgumentError:
        usage()
        sys.exit(-1)

    if 'usage' in args and args['usage']:
        help()
        sys.exit(0)
    elif 'directory' in args and not args['directory']:
        usage()
    else:
        work_dir = args['directory']
        print "work_dir: ", work_dir
        if not os.path.exists(work_dir):
            raise Exception("Directory '%s' does not exist" % work_dir)
        sys.exit(merge(**args))


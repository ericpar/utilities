#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""This file is a slight adaptation from a recipe that's available on
ActiveState's website:

http://code.activestate.com/recipes/577739/ (r4)


Dependencies
------------

- NumPy


Author: Eric Parent <eric@eparent.info>

Copyright (c) 2013 Eric Parent

License: The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
# ----------------------------------------
# Standard libraries
# ----------------------------------------
from xml.dom.minidom import Document
import copy


class dict2xml(object):
    """This class converts a dictionary to XML."""

    def __init__(self, structure):
        self.doc = Document()
        if len(structure) == 1:
            rootName = str(structure.keys()[0])
            self.root = self.doc.createElement(rootName)

            self.doc.appendChild(self.root)
            self.build(self.root, structure[rootName])

    def build(self, father, structure):
        if type(structure) == dict:
            for k in structure:
                tag = self.doc.createElement(k)
                father.appendChild(tag)
                self.build(tag, structure[k])

        elif type(structure) == list:
            grandFather = father.parentNode
            tagName = father.tagName
            grandFather.removeChild(father)
            for l in structure:
                tag = self.doc.createElement(tagName)
                self.build(tag, l)
                grandFather.appendChild(tag)

        else:
            data = str(structure)
            tag = self.doc.createTextNode(data)
            father.appendChild(tag)

    def display(self):
        """Returns a pretty-print version of the current XML
        document.

        """
        print self.doc.toprettyxml(indent="  ")


def main():
    example = {'sibbling':
               {'couple':
                {'mother': 'mom',
                 'father': 'dad',
                 'children': [{'child':'foo'},
                              {'child':'bar'}]},
            }
           }

    try:
        import numpy as np
        width = 10
        example = {'drift':
                   {'sid':
                    {'Blue': [np.arange(10, 10 + width),
                              np.arange(44, 44 + width)],
                     'Red': [np.arange(7, 7 + width),
                             np.arange(22, 22 + width)],
                 },
                    'cubenames': ["123456789s0", "123456987s0"],
                },
               }
    except ImportError:
        print "The NumPy module is not available on this system."
    xml = dict2xml(example)
    xml.display()

# ------------------------------------------------------------
# Standalone version
# ------------------------------------------------------------
if __name__ == '__main__':
    sys.exit(main())
#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""Module providing with backup facilities of parts of an HDF5 file.

Created on 2012-06-28

Author: Eric Parent <eric@eparent.info>

Dependencies
------------

- PyTables

- Log

- ErrorCodes

- Hypercube (to be removed)

- common.misc (to be removed)

To do
-----

- Remove dependency towards Hypercube

- Remove dependency towards common.misc



Copyright (c) 2013 Eric Parent

License: The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


"""

# -----------------------------
# Standard library imports
# -----------------------------
import os
import shutil

# ------------------------------
# Third party library imports
# ------------------------------
import tables as tb

# -----------------------------
# Local library imports
# -----------------------------
import fmgr.hypercube as hypercube
from errors import ErrorCodes
from common import misc
from utils.Log import Logger

# Enable logging with PKTracer
log = Logger(channel='backup')


# ------------------------------------------

def manage_backup(cube, backup, node_names, failure=False, deepcopy=False):
    """ASSERTION: the dirty flag was cleared out upon completion of
     upgrade steps

    Arguments:
    ----------

    - cube: the cube for which a backup was made

    - backup: the fully-resolved path to the backup

    - node_names: a list of backup nodes to be restored if required

    - failure: boolean indicating if a failure was signified

    - deepcopy: boolean indicating if the backup was following the
      deep copy strategy

    """
    flag = cube.is_dirty()
    cube.close()
    if flag or failure:
        # Error occured during the processing...
        msg = "Cube '%s' found dirty or failure occured: restoring the backup if possible" % cube.cubename
        log.warning(msg, tab=1)
        if restore_backup_copy(backup, node_names, deepcopy=deepcopy):
            msg = "Successfully restored backup"
            log.info(msg, tab=1)
        else:
            # The backup restoration failed...
            msg = "Could not restore backup copy '%s'" % backup
            return ErrorCodes("BackupFailed", log=log, msg=msg, fatal=True)
        # The backup restoration proceeded successfuly but the restore
        # is due to a failure
        return ErrorCodes("Success")
    else:
        # Everything completed successfully, hence cleaning up: if
        # then take the copy as the upgraded cube
        if os.path.exists(backup):
            os.remove(backup)
        return ErrorCodes("Success")


def _node_exists(h5file, node_name):
    try:
        h5file.getNode(node_name)
        node_exists = True
    except tb.NoSuchNodeError:
        node_exists = False
    return node_exists


def create_backup_copy(cube, node_names, deepcopy=False):
    """Given an instance of Hypercube, creates a copy of the H5 file
    and returns the name of the newly created file.

    Arguments:
    ----------

    - path: the path to the hypercube for which to get a working copy

    - node_names: a list of nodes to back up

    - deepcopy: boolean indicating if the backup is actually on a
      file-copy level


    Returns the name of the working copy H5 file.

    """
    path = cube.path
    _dir = os.path.dirname(path)
    _fname = os.path.basename(path)
    parts = _fname.split('.')
    extn = parts[-1]
    prefix = '.'.join(parts[:-1] + ['BACKUP', extn])
    dst = os.path.join(_dir, prefix)
    msg = "Creating backup file: %s" % dst
    log.info(msg, tab=1)

    if deepcopy:
        # behaviour of a deep copy backup
        shutil.copyfile(path, dst)
    else:
        backup_h5 = tb.openFile(dst, mode="a")
        # Copy the node into the backup file
        if "/backup" not in backup_h5:
            backup_h5.createGroup("/", "backup", title="", filters=None)
        for node_name in node_names:
            if _node_exists(cube.getH5(), node_name):
                msg = "Copying the node '%s' into backup" % node_name
                cube.getH5().copyNode(node_name,
                                      backup_h5.getNode("/backup"),
                                      recursive=True,
                                      overwrite=True)
                log.debug(msg)
        backup_h5.flush()
        backup_h5.close()
    cube.close()
    return dst


def _original_name(path):
    """Returns the original, fully-resolved path of the backup file.

    Argument:
    ---------

    - path: the fully-resolved path of a backup file

    """
    working = path
    items = working.split('.')
    items.remove('BACKUP')
    return '.'.join(items)


def restore_backup_copy(backup, node_names, deepcopy):
    """Restores the backup copy of a Hypercube.

    If the given cube isn't a backup copy, then raise a PKException.

    Arguments:
    ----------

    - backup: the path to the backup copy of a hypercube

    - node_names: a list of nodes to restore

    - deepcopy: boolean indicating if the backup is a file-copy

    Returns the fully-resolved path of the Hypercube that was the
    working copy.

    """
    restored = False
    if backup is None:
        msg = "No backup copy could be made, so no restoration is possible"
        log.error(msg)
        ErrorCodes("NoBackupCopyAvailable")
        return False

    if "BACKUP" not in backup:
        msg = "%s is not a backup copy" % backup
        ErrorCodes("NotABackupCopy", msg=msg, log=log, fatal=True)
    restored = _original_name(backup)

    if deepcopy:
        # Handling the deepcopy strategy, i.e. file copy
        if os.path.exists(restored):
            os.remove(restored)
        os.rename(backup, restored)
    else:
        backup_h5 = tb.openFile(backup, mode="r")
        restored_h5 = tb.openFile(restored, mode="a")
        msg = "Restoring data from %s" % (backup,)
        log.debug(msg)
        _successful = False
        try:
            where = "/backup"
            for node in backup_h5.iterNodes(where,
                                            classname=None):
                backup_h5.copyNode(node,
                                   restored_h5.getNode("/other"),
                                   recursive=True,
                                   overwrite=True)
            # Restore the attributes
            _successful = True
        finally:
            restored_h5.close()
            backup_h5.close()

        if _successful:
            os.remove(backup)
    return restored

def copy_to_clear_space(path):
    """Use tb.copyFile to copy file, clears space from deleted nodes"""
    tmp_path = path + '.tmp'
    if os.path.lexists(tmp_path):
        os.remove(tmp_path)
    os.rename(path, tmp_path)
    tb.copyFile(tmp_path, path)
    os.remove(tmp_path)
    msg = "Copying file to clear space from deleted nodes, path: %s" % path
    log.debug(msg)


# ========================================

def super_main(directory, cubename, 
               node_paths, 
               processing_func, 
               process_name,
               **kwargs):
    """
    Super function for controlling backup and main processing run

    Arguments:
    ----------
        - directory: the directory where to create the file and where
        the 'fits' file is located.

        - cubename: name of the cube to encapsulate.

        - processing_func: the function which carries out the processing
                           (usually called sub_main)

        - process_main: the string representation of the processing for printing
                        (e.g., 'mask', 'geolocate')
    """
    # Process keywords
    kwds = {'verbose': 1,
            'subdir': misc.post}
    kwds.update(kwargs)

    # Create a backup copy
    _path = hypercube.Hypercube.build_path(directory, cubename, **kwds)
    if os.path.exists(_path):
        # A backup is created only if the cube exists
        cube = hypercube.Hypercube(directory, cubename, subdir=kwds['subdir'])
        try:
            cubename = cube.cubename
        except IOError:
            msg = "HDF5 file not found at: %s" % \
                  hypercube.Hypercube.build_path(directory, cubename, **kwds)
            ErrorCodes('MissingHdf5File', msg=msg, log=log, fatal=True)
        finally:
            cube.close()
        backup = create_backup_copy(cube, node_paths)
    else:
        backup = None

    _passed = True   # default value
    try:
        # Run processing function
        _passed = processing_func(directory, cubename, **kwds)
    except Exception, exc:
        msg = "ERROR -- problem during execution of function '%s': %s" \
              % (processing_func.__name__, exc)
        log.error(msg)
        _passed = False

    # Instantiate the cube and restore the backup
    try:
        cube = hypercube.Hypercube(directory, cubename, subdir=kwds['subdir'])
    except IOError:
        msg = "HDF5 file not found at: %s" % \
              hypercube.Hypercube.build_path(directory, cubename, **kwds)
        ErrorCodes('MissingHdf5File', msg=msg, log=log, fatal=True)

    try:
        #
        # ASSERTION: the dirty flag was cleared out upon completion of
        # the processing steps
        #
        _failure = not _passed
        cubepath = cube.path
        status = manage_backup(cube, backup, node_paths, failure=_failure)
        if status == ErrorCodes('Success'):
            if _passed:
                msg = '%s %s succeeded.' % (cubename, process_name)
                log.debug(msg)
            else:
                _failure = True
                msg = '%s %s failed.\n' % (cubename, process_name)
                log.info(msg)
                # Setting the geolocation attribute to '0'
                cube = hypercube.Hypercube(abs_path=cubepath)
                cube.save_attr_dict({'state_%s' % process_name: 0})
                cube.save()
                # Printing status message
        else:
            _failure = True
            msg = '%s %s failed.' % (cubename, process_name)
            log.debug(msg)
        #
        # If failure occured, notify the caller with a clear message
        #
        if _failure:
            status = ErrorCodes("%sFailed" % process_name.capitalize(),
                                msg=msg,
                                log=log,
                                fatal=True)
    finally:
        cube.close()

    return (status == ErrorCodes('Success'))


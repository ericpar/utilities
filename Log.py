#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""Created on 2013-05-06

Author: Eric Parent <eric@eparent.info>

Copyright (c) 2013 Eric Parent

License: The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

# -----------------------------
# Standard library imports
# -----------------------------
import os
import sys
import inspect
import tempfile
import logging
import unittest


class Logger(object):
    """Encapsulates the PKTracer interface and provides a logging
    utility class for the whole Backend module.

    The generated log file path follows this pattern:

    <tempdir>\logs\Backend.log

    Usually, on a Windows OS, the <tempdir> resolves to the following
    location:

    C:\Users\<username>\AppData\Local\Temp

    A "channel" is a name to be used towards which we direct some
    messages. It can be regarded as a publication. The log file can
    handle many publications and are differentianted by the presence
    of the publication name on each line. Thus the filtering of a log
    file per channel (or publication) is easy.

    """

    DefaultChannel = "Backend"    # The default channel
    Formatter = logging.Formatter('%(asctime)s %(name)-20s %(levelname)-8s %(message)s')


    def __init__(self, channel=DefaultChannel, log_level=logging.INFO, filename=None):
        """Initializes the current instance of Logger.

        Pre-conditions
        --------------

        Using the "LOG_DIR" environment variable if it exists. If not,
        then 'TEMP' and finally the system's temporary directory is
        used as a place to write the log file.

        The current execution process needs to have the write
        credentials to write the log file.

        """
        self.__tempdir = tempfile.gettempdir()
        self.dirname = os.path.join(self.__tempdir, 'logs')
        if not os.path.exists(self.dirname):
            os.makedirs(self.dirname)
        self.__channel = channel
        self.__logger = self.__init_logger(name="Backend", log_level=log_level, filename=filename)

    def __determine_log_file(self, module_name="Backend"):
        '''Given a module's name, this function determines the best file
        (and directory) where to save the logging output.

        If no module name is given, then BackEnd is being used a file
        name's prefix.

        '''
        if module_name is not None:
            log_dir = os.path.dirname(module_name)
            name, ext = os.path.splitext(os.path.basename(module_name))
        else:
            name = "BackEnd"
        if "LOG_DIR" in os.environ:
            log_dir = os.environ["LOG_DIR"]
        elif "TEMP" in os.environ:
            log_dir = os.environ["TEMP"]
        else:
            # Retrieving the system's temp directory
            import tempfile
            log_dir = tempfile.gettempdir()
        return os.path.join(log_dir, "%s.log" % name)

    def __init_logger(self, name, log_level=logging.INFO, filename=None):
        """Utility function to trace the script's command-line arguments.

        This method will produce a log file in directory pointed out by
        "LOG_DIR" environment variable if it exists or in the script's
        directory otherwise.

        The logging level being used is DEBUG.

        The file level logger is being set to logging.DEBUG

        Arguments:
        ----------

        - args: the arguments to be sent to trace.

        - log_level: Logging level at which the logging stops (inclusive)

        - filename: filename of the log file
        """
        # Retrieve the logger
        if filename is None:
            self.logfile = self.__determine_log_file(name)
        else:
            dirname = os.path.dirname(filename)
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            self.logfile = filename
        logger = logging.getLogger(name)
        logger.setLevel(log_level)

        # create formatter
        fh = logging.FileHandler(self.logfile)
        fh.setLevel(log_level)
        fh.setFormatter(Logger.Formatter)
        logger.addHandler(fh)
        self.__logger = logger
        return self.__logger

    def info(self, msg, func=None, tab=0):
        """Logs the given message to the "info" logging level. If
        given a function name, then this is used for the logging
        purposes. Otherwise, the name of the caller function is used.

        This logging level is useful for logging usual information,
        like the completion of some portions of the program.

        For instance, if the computations require multiple
        sub-processing to occur, it would be a good idea to save the
        start/end of each intermediate sub-processing's execution.

        This logging level is not meant for errors, warnings or fine
        details of computations like low-level numerical values. But
        it can be used for high-level informations or metrics, like
        standard deviation or average, like number of items to be
        treated by a specific process, etc.

        Arguments:
        ----------

        - msg: the message to be logged

        - func: <optional> if given, it is being used as the function
          name to show up on the log line

        Post-condition:
        ---------------

        The log file is updated with the message and a function
        name.

        """
        if self.__logger is not None:
            _fname = func or inspect.stack()[1][3]
            self.__logger.info(msg)
        print ("\t" * tab) + msg

    def debug(self, msg, func=None):
        """Logs the given message to the "debug" logging level. If
        given a function name, then this is used for the logging
        purposes. Otherwise, the name of the caller function is used.

        This logging level is useful for logging information that
        would be useful for debugging a system, mostly during its
        development phase.

        For instance, it could be used for logging some intermediate
        results and even some details about particular values or
        context.

        This logging level is not meant for errors, warnings or usual
        message.

        It is meant to be used for logging some details that make
        sense to developers and systems designers.

        *NOTE*: Keep in mind that these logs could simply be disabled
         in production context. Therefore having support critical
         information in that logging level may not be available.

        Arguments:
        ----------

        - msg: the message to be logged

        - func: <optional> if given, it is being used as the function
          name to show up on the log line

        Post-condition:
        ---------------

        The log file is updated with the message and a function
        name.

        """
        if self.__logger is not None:
            _fname = func or inspect.stack()[1][3]
            self.__logger.debug(msg)

    def warning(self, msg, func=None, tab=0):
        """Logs the given message to the "warning" logging level. If
        given a function name, then this is used for the logging
        purposes. Otherwise, the name of the caller function is used.

        This logging level is useful for logging unusual information
        that could lead to unexpected behaviour. It could also be used
        for notifying deprecated values.

        Arguments:
        ----------

        - msg: the message to be logged

        - func: <optional> if given, it is being used as the function
          name to show up on the log line

        Post-condition:
        ---------------

        The log file is updated with the message and a function
        name.

        """
        if self.__logger is not None:
            _fname = func or inspect.stack()[1][3]
            self.__logger.warning(msg)
        print ("\t" * tab) + "WARNING - " + msg

    def error(self, msg, func=None, tab=0):
        """Logs the given message to the "error" logging level. If
        given a function name, then this is used for the logging
        purposes. Otherwise, the name of the caller function is used.

        This logging level is useful for logging *recoverable errors*,
        like when handling properly an error in a try/catch
        block. Such error can lead to a more serious error but not at
        the level where the error is handled.

        Arguments:
        ----------

        - msg: the message to be logged

        - func: <optional> if given, it is being used as the function
          name to show up on the log line

        Post-condition:
        ---------------

        The log file is updated with the message and a function
        name.

        """
        if self.__logger is not None:
            _fname = func or inspect.stack()[1][3]
            self.__logger.error(msg)
        print ("\t" * tab) + "ERROR - " + msg

    def critical(self, msg, func=None, tab=0):
        """Logs the given message to the "fatal" logging level. If
        given a function name, then this is used for the logging
        purposes. Otherwise, the name of the caller function is used.

        This logging level is useful for logging *unrecoverable
        errors*, that will cause the program to exit.

        Arguments:
        ----------

        - msg: the message to be logged

        - func: <optional> if given, it is being used as the function
          name to show up on the log line

        Post-condition:
        ---------------

        The log file is updated with the message and a function
        name.

        """
        if self.__logger is not None:
            _fname = func or inspect.stack()[1][3]
            self.__logger.critical(msg)
        print ("\t" * tab) + "CRITICAL ERROR - " + msg


class FunctionWrapper(object):
    """This class wraps a function to be logged in the logger.
    @logwrap
    def doSomething(spam, eggs, foo, bar):
        print "Doing something totally awesome with %s and %s." % (spam, eggs)

    doSomething("beans","rice", foo="wiggity", bar="wack")

    Run it, it will yield the following output:

    C:\scripts>python decoratorExample.py

    Calling doSomething with arguments ('beans', 'rice') and named
    arguments {'foo': 'wiggity', 'bar': 'wack'} Doing something
    totally awesome with beans and rice.

    NOTE: From
    http://stackoverflow.com/questions/218616/getting-method-parameter-names-in-python

    """

    def __init__(self, function):
        """Initializer of the FunctionWrapper class.

        Arguments:
        ----------

        - function: the function to be wrapped

        """
        self.function    = function
        self._init_logger()

    def logAndCall(self, *arguments, **namedArguments):
        """This method logs the arguments given to the wrapped function and
        executes it right after.

        Arguments:
        ----------
        - arguments: the arguments given to the wrapped function.
        - named arguments: the named arguments given to the wrapped
        function.

        """
        args = str(tuple([str(a) for a in arguments]))
        msg = "Calling '%s' with args: %s; keyword arguments: %s" \
              % (self.function.__name__,
                 args,
                 str(namedArguments))
        self.logger.info(msg, self.function.__name__)
        self.function.__call__(*arguments, **namedArguments)

    def _init_logger(self):
        """Utility function to trace the script's command-line
        arguments.

        This method will produce a log file in directory pointed
        out by "LOG_DIR" environment variable if it exists or in
        the script's directory otherwise.
        """
        self.logger = Logger()

#
# Decorator exported by this module.  The usage of this wrapper is
# described in the LogWrappers.Function class
#
def logfunctionargs(function):
    """Decorator that enables a function to log its parameters
    (positional and named).

    Logging is made in $LOG_DIR/<filename>.log if LOG_DIR environment
    variable is defined or in the script's directory otherwise (see:
    FunctionWrapper class).

    """
    return FunctionWrapper(function).logAndCall


# ---------------------------------------
#      Standalone version of script
# ---------------------------------------
@logfunctionargs
def _test_fn(a, b, *args, **kwargs):
    print 'Done...'
    return 0


class Simple_Test(unittest.TestCase):

    def test_find_messages(self):
        """Simple test function for validating the use of the Log.Logger
        class.
        
        """
        try:
            logger = Logger(log_level=logging.DEBUG)
            logfile = logger.logfile
            messages = []
            # Info message
            msg = "This is my very first line"
            logger.info(msg)
            messages.append(msg)
            # Debug message
            msg = "Here is a debug line"
            logger.debug(msg)
            messages.append(msg)
            # Warning message
            msg = "How about a warning line?"
            logger.warning(msg)
            messages.append(msg)
            # Function call
            _test_fn(1, 'a', 123, 321, '', toto='toto', foo=77)
            messages.append("args: ('1', 'a', '123', '321', ''); keyword arguments: {'foo': 77, 'toto': 'toto'}")
            print "logfile that was created: %s" % logger.logfile
            with open(logger.logfile, "r") as logfile:
                validated_msg = []
                for msg in messages:
                    for line in logfile:
                        # Check the occurence of the message file...
                        if msg in line:
                            print "found this message '%s'" % msg
                            validated_msg.append(msg)
                            break

            msg_not_found = list(set(messages) - set(validated_msg))
            self.assertEqual(len(msg_not_found),
                                 0,
                                 "These messages were not encountered: %s" % str(msg_not_found))
        except Exception, exc:
            self.fail("No exception should be encountered: %s" % exc)
        return 0

#
# Standalone version of this script...
#
if __name__ == '__main__':
    unittest.main()
#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""Functions for manipulating images.


Dependencies
------------

- Python 2.7 (argparse)

- NumPy

- SciPy


Author: Eric Parent <eric@eparent.info>

Copyright (c) 2012 Eric Parent

License: The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

# Notes: It's expected that the points are listed as point-corner-point.

# -----------------------------
# Standard library imports
# -----------------------------
import sys
import os
import datetime as dt
import argparse

# -----------------------------
# Third party library imports
# -----------------------------
import numpy as np
import scipy.ndimage as nd
from scipy.misc import imread, imsave

parser = argparse.ArgumentParser(description="Produces a report for a given set of cubes.")
parser.add_argument('-R', '--directory',
                    action="store",
                    type=str,
                    required=1,
                    dest='directory',
                    help='root cube directory')


def main(args):
    """Dealing with the rotation of images.
    """
    angle = -90.0
    files = os.listdir(args['directory'])
    n = 0
    print "Started rotating files "
    for image in files:
        if ".jpeg" in image or ".png" in image:
            n += 1
            _image = os.path.join(args['directory'], image)
            img = imread(_image)
            imsave(_image, nd.rotate(img, angle, reshape=1, order=0))
    print "Rotated %d different images" % n
    return 0

#
# -- Standalone version
#
if __name__ == '__main__':
    args = vars(parser.parse_args())
    sys.exit(main(args))
